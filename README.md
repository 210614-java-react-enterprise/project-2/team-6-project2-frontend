# Trasker Task Management System

## Project Description

The Trasker system is intended to be a lightweight, simple-to-use, browser-based organization utility. Like a virtual checklist, users can create, view, and complete tasks, by providing as much or as little data as they would like to the application. Additionally, at the user's discretion, they can quickly and easily push or pull tasks from their Google Calendar at the push of a button.

## Technologies Used

* Java
* Jenkins
* Spring Framework
* AWS EC2
* AWS S3
* JavaScript
* React
* JUnit
* Docker
* Maven
* GitLab
* PostgreSQL
* DBBeaver

## Features

* User can create a new account
* User can log in
* User can add new tasks and assign a date and time to a task
* User can add subtasks to their tasks as they create them
* User can view all of the tasks they've added to the system
* User can naviate to different dates to see tasks on those dates

## Getting Started

### If server is running:

* Go to http://project2-team6.s3-website-us-east-1.amazonaws.com/ and register a new account.

### If server is not running:

* `git clone https://gitlab.com/210614-java-react-enterprise/project-2/team-6-project2-frontend.git`
* In  this folder, run `npm install`
* Change the connection endpoints to localhost.

* `git clone https://gitlab.com/210614-java-react-enterprise/project-2/team-6-project2.git`
* In this folder, run `mvn clean package`
* Build and run main function in Driver.java.

* In frontend folder, run `npm start`



## Backend Repository

* https://gitlab.com/210614-java-react-enterprise/project-2/team-6-project2.git

## Contributors

* Kalvin Miller
* Apurv Patel
* Conor Scalf
