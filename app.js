const root = document.getElementById("app-root");
window.addEventListener("load", route);
window.addEventListener("hashchange", route);


// when #/day -> fetch "components/daily-view/daily-view.component.html"
const routes = [
    {path: "", componentFileName: "home"},
    {path: "/", componentFileName: "home"},
    {path: "#/day", componentFileName: "daily-view"},
    {path: "#/week", componentFileName: "weekly-view"},
    {path: "#/month", componentFileName: "monthly-view"},
    {path: "#/new-task", componentFileName: "new-task"},
    {path: "#/login", componentFileName: "login"},
    {path: "#/register", componentFileName: "register-user"}
]


function route(){
    const hashPath = location.hash;
    const currentRoute = routes.find(route=>route.path===hashPath);

    
    const viewName = currentRoute ? currentRoute.componentFileName : "home";
    renderView(viewName);
}

function renderView(view){
    fetch(`components/${view}/${view}.component.html`)
        .then(response=>response.text())
        .then(htmlBody=>{
            root.innerHTML = htmlBody;
            loadScript(view);
        });
}

function loadScript(scriptName){
    let localScript = document.getElementById("dynamic-js");
    if (localScript){
        localScript.remove();
    }
    localScript = document.createElement("script");
    localScript.id = "dynamic-js";
    localScript.src = `components/${scriptName}/${scriptName}.component.js`
    localScript.type = "module";
    document.body.appendChild(localScript);
}
