const navbar = document.getElementById("navbar");

//TODO: The filepath for accessing the navbar html is probably wrong. Fix it
let renderNavbar = () => {
    fetch(`components/navbar/navbar.component.html`)
        .then(response => response.text())
        .then(htmlBody => {
            navbar.innerHTML = htmlBody;
            loadNavbarScript();
        })
}


//TODO: Change this so that it highlights the daily view selection when it loads the new navbar
let loadNavbarScript = () => {
    let prevNavbarScript = document.getElementById("navbar-script");
    if (prevNavbarScript){
        prevNavbarScript.remove();
    }
    let newNavbarScript = document.createElement("script");
    newNavbarScript.id = "navbar-script";
    newNavbarScript.src = `components/navbar/navbar.component.js`
    newNavbarScript.type = "module";
    document.body.appendChild(prevNavbarScript);
}