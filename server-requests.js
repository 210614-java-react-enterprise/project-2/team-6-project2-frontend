const BASE_URL = "http://3.230.1.183:8082";

function sendAjaxRequest(method, url, body, requestHeaders, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    for(let requestHeaderKey in requestHeaders){
        // {"Authorization" : "auth-token", "Content-Type": "application/json"}
        xhr.setRequestHeader(requestHeaderKey, requestHeaders[requestHeaderKey]);
    }
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
          console.log(xhr);
        if (xhr.status>199 && xhr.status<300) {
          successCallback(xhr);
        } else {
          failureCallback(xhr);
        }
      }
    };
    if(body){
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function sendAjaxGet(url, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("GET", url, null, requestHeaders, successCallback, failureCallback);
}

function ajaxGetTracks(authToken, successCallback, failureCallback){
    let requestUrl = `${BASE_URL}/tracks`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function sendAjaxPost(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("POST",url, body, requestHeaders, successCallback, failureCallback);
}

function ajaxCreateTask(body, authToken, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = `${BASE_URL}/new-task`;
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded"};
    let requestUrl = `${BASE_URL}/login`
    let requestBody = `username=${username}&password=${password}`;
    sendAjaxPost(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}

export {ajaxGetTracks, ajaxCreateTrack, ajaxLogin};
