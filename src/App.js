import './App.css';
import WelcomePage from './components/WelcomePage';
import RegisterUser from './components/register/RegisterUser';
import React from "react";
import { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Login from './components/login/Login';
import ViewCalendar from './components/calendar-view/ViewCalendar';


function App() {
  //const date ="July 29, 2021";
  

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(dd<10)
  {
      dd='0'+dd;
  }
  if(mm<10)
  {
      mm='0'+mm;
  }
  let todayString = yyyy+'-'+mm+'-'+dd;
  
  let [date, setDate] = useState(todayString);
  /*
  const tasks = [
    {
      text : "First task",
      subtasks : [
        {text: "First Subtask"},
        {text: "Second Subtask"},
        {text: "Third Subtask"}
      ]
    },
    {
      text : "Second task",
      subtasks : [
        {text: "First Subtask"},
        {text: "Second Subtask"},
        {text: "Third Subtask"}
      ]
    },
    {
      text : "Third task",
      subtasks : [
        {text: "First Subtask"},
        {text: "Second Subtask"},
        {text: "Third Subtask"}
      ]
    }
  ]
  */

  let [user, setUser] = useState({});
  let [tasks, setTasks] = useState([]);



  let handleSetUser = (_userId, _username, _password) =>{
    setUser({userId: _userId, username: _username, password: _password})
  }

  let handleSetTasks = (_tasks) =>{
    setTasks(_tasks)
  }

  //TODO: finish this function. I'm tired lol
  let goToNextDate = (dateString) =>{
    let year = parseInt(dateString.substring(0, 4));
    let month = parseInt(dateString.substring(5,7));
    let day = parseInt(dateString.substring(8));
    
    let thisDay = new Date(year, month - 1, day);
    let nextDay = new Date(thisDay);
    nextDay.setDate(thisDay.getDate() + 1);

    console.log(`Going to next date. dateString: ${dateString}, year ${year}, month: ${month}, day: ${day}. thisDay: ${thisDay}, nextDay: ${nextDay}`);

    let dd = nextDay.getDate();
    let mm = nextDay.getMonth()+1;
    let yyyy = nextDay.getFullYear();
    if(dd<10)
    {
        dd='0'+dd;
    }
    if(mm<10)
    {
        mm='0'+mm;
    }
    let nextDayString = yyyy+'-'+mm+'-'+dd;
    console.log(`nextDayString: ${nextDayString}`);
    return nextDayString;
  }

  let goToPrevDate = (dateString) =>{
    let year = parseInt(dateString.substring(0, 4));
    let month = parseInt(dateString.substring(5,7));
    let day = parseInt(dateString.substring(8));
    
    let thisDay = new Date(year, month - 1, day);
    let prevDay = new Date(thisDay);
    prevDay.setDate(thisDay.getDate() - 1);

    console.log(`Going to previous date. dateString: ${dateString}, year ${year}, month: ${month}, day: ${day}. thisDay: ${thisDay}, nextDay: ${prevDay}`);

    let dd = prevDay.getDate();
    let mm = prevDay.getMonth()+1;
    let yyyy = prevDay.getFullYear();
    if(dd<10)
    {
        dd='0'+dd;
    }
    if(mm<10)
    {
        mm='0'+mm;
    }
    let prevDayString = yyyy+'-'+mm+'-'+dd;
    console.log(`prevDayString: ${prevDayString}`);
    return prevDayString;
  }

  let handleChangeDate = (direction) =>{
    if(direction == "next"){
      let nextDay = goToNextDate(date);
      setDate(nextDay);
    } else if(direction == "prev"){
      let prevDay = goToPrevDate(date);
      setDate(prevDay);
    }
  }

    return (
      <Router>
        <Route path="/" exact component={WelcomePage}/>
        <Route path="/login" render={(props)=><Login {...props} onSetUser={handleSetUser} onSetTasks={handleSetTasks}/>}/>
        <Route path="/register" component={RegisterUser} />
        <Route 
          path="/viewCalendar" 
          render={(props) => <ViewCalendar {...props} date={date} tasks={tasks} user={user} onChangeDate={handleChangeDate} onSetTasks={handleSetTasks}/>}
          />
      </Router>
      );
}

export default App;
