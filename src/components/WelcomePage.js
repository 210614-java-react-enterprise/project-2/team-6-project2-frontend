import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";
// import { Nav } from "react-bootstrap";
import RegisterUser from "./register/RegisterUser";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import React from "react";

export default function WelcomePage(props){
    return (
    <>
        <Button variant="primary" href="/login" size="lg">Log In</Button>{'    '}
        <Button variant="success" href="/register" size="lg">Register</Button>
    </>

   );
}