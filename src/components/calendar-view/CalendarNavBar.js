import Nav from "react-bootstrap/Nav";
import { Navbar } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Collapse } from "react-bootstrap";
import { useState } from "react";
import CreateTask from "../task/CreateTask";
import CreateTaskForm from "../task/CreateTaskForm";
import { withRouter } from "react-router-dom";
import axios from "axios";

//TODO: Make it look like the commentted out code, but use react-bootstrap instead of bootstrap
function CalendarViewNavBar(props) {

    let setDaily = () =>{
        props.setViewMode("daily");
    }
    let setWeekly = () =>{
        props.setViewMode("weekly");
    }
    let setMonthly = () =>{
        props.setViewMode("monthly");
    }

    let [openCreateTaskForm, setOpenCreateTaskForm] = useState(false);

    let toggleTaskForm = () =>{
        setOpenCreateTaskForm(!openCreateTaskForm);
    }

    let logout = () => {
        sessionStorage.clear();
        props.history.push("/");
    }
    
    let onSynchWithChrome = () => {
        axios.get(`http://3.230.1.183:8082/tasks?id=${props.user.userId}`,  
        {headers:{"Content-Type": "application/json", "Authorization": `${props.user.username}-auth-token`}})
            .then( response=>{
                let synchRequestBody = response.data;
                console.log(JSON.stringify(synchRequestBody));
                axios.post("http://3.230.1.183:8082/sync", 
                    synchRequestBody, 
                    {headers:{"Content-Type": "application/json"}})
                    .then( syncResponse => {
                        console.log(`syncResponse: ${syncResponse}`);
                    })
            })
    }
    
    return (
        <>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand>Task Tracker</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    <Nav.Link onClick={setDaily} active={props.viewMode == "daily"}>Daily View</Nav.Link>
                    {/* 
                    <Nav.Link onClick={setWeekly} active={props.viewMode == "weekly"}>Weekly View</Nav.Link>
                    <Nav.Link onClick={setMonthly} active={props.viewMode == "monthly"}>Monthly View</Nav.Link>
                    */}
                    <Nav.Link onClick={toggleTaskForm} active={openCreateTaskForm}> Create New Task  </Nav.Link>
                    <Nav.Link onClick={logout}>Log Out</Nav.Link>
                    </Nav>
                    <Nav>
                    <Nav.Link>
                        <Button variant="primary" onClick={onSynchWithChrome}>Synch With Google Calendar</Button>
                    </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                
            </Container>

        </Navbar>

        <Collapse in={openCreateTaskForm}>
            <Container>
                <CreateTask user={props.user} tasks={props.tasks} onSetTasks={props.onSetTasks} toggleTaskForm={toggleTaskForm}/>
            </Container>
        </Collapse>
        </>
    );
    {/*<nav class="navbar navbar-expand-lg navbar-dark bg-dark" aria-label="Tenth navbar example">
        <button type="button" class="btn">Task Tracker</button>
        <div class="container-fluid">
            <button 
                class="navbar-toggler" 
                type="button" 
                data-bs-toggle="collapse" 
                data-bs-target="#navbarsExample08" 
                aria-controls="navbarsExample08" 
                aria-expanded="false" 
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
    
          <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
                <ul class="navbar-nav">
                  <li class="nav-item active">
                      <a class="nav-link" aria-current="page" href="#/day">Daily View</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" aria-current="page" href="#/week">Weekly View</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" aria-current="page" href="#/month">Monthly View</a>
                  </li>
                  
                  <li class="nav-item">
                      <a class="nav-link" aria-current="page" href="#/home">Log Out</a>
                  </li>
                </ul>
            </div>

            <button id="google-calendar-btn" type="button" class="btn btn-primary float-right">
              Synch with Google Calendar
            </button>

        </div>
      </nav>*/}
  
}

export default withRouter(CalendarViewNavBar);