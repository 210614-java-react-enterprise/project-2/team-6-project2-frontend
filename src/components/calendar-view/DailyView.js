
import Card from "react-bootstrap/Card";
import Task from "../task/Task";
import { Container } from "react-bootstrap";
import { Button } from "react-bootstrap";

export default function DailyView(props){
    return (
    <>
        <Container>
            <Button variant="info" onClick={()=>props.onChangeDate("prev")}>Go to previous day</Button>
            <div style={{float: 'right'}}>
                <Button variant="info" onClick={()=>props.onChangeDate("next")}>Go to Next day</Button>
            </div>
            <Card>
                <Card.Body>
                    <Card.Title>{props.date}</Card.Title>
                    {props.tasks.length > 0 ? 
                        props.tasks.map((task, index)=> <Task key={index} task={task}/>) :
                        <h2>There are no tasks scheduled this day</h2>}
                </Card.Body>
            </Card>
        </Container>
    </>
    );
}