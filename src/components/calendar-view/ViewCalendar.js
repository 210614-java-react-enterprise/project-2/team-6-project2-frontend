import CalendarViewNavBar from "./CalendarNavBar";
import DailyView from "./DailyView";
import WeeklyView from "./WeeklyView";
import MonthlyView from "./MonthlyView";
import { useState } from "react";

export default function ViewCalendar(props){
    const [viewMode, setViewMode] = useState("daily");

    let viewComponent;
    
    switch(viewMode){
        case "daily":
            let dailyTasks=props.tasks.filter((task)=>{
                return task.taskDate == props.date})
            console.log(`props.tasks: ${JSON.stringify(props.tasks)}, dailyTasks: ${JSON.stringify(dailyTasks)}`);
            
            viewComponent  = <DailyView date={props.date} onChangeDate={props.onChangeDate} tasks={dailyTasks}/>
            break;
        case "weekly":
            viewComponent = <WeeklyView {...props}/>
            break;
        case "monthly":
            viewComponent = <MonthlyView {...props}/>
            break;
        default:
            break;
    }

    return (
        <>
            <CalendarViewNavBar viewMode={viewMode} setViewMode={setViewMode} user={props.user} tasks={props.tasks} onSetTasks={props.onSetTasks}/>
            {viewComponent}
        </>
    );
}