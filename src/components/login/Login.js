import axios from "axios";
import { useState } from "react";
import { withRouter } from "react-router-dom";
import LoginForm from "./LoginForm";

//TODO: When a user logs in, we need to get their userID to use it to submit tasks and stuff
function Login(props){
    const [credentials, setCredentials] = useState("");

    const handleChange = (e)=>{
        const {name, value} = e.target;
        setCredentials({...credentials, [name]:value});
    }

    const handleSubmit = (e)=>{
        e.preventDefault();

        const requestBody = `username=${credentials.username}&password=${credentials.password}`;
        console.log(requestBody);
        axios.post("http://3.230.1.183:8082/login",requestBody, {headers:{"Content-Type": "application/x-www-form-urlencoded"}})
            .then(userResponse=>{
                console.log("successful login!!!");
                console.log(userResponse);
                
                sessionStorage.setItem("token", userResponse.headers["authorization"]);

                props.onSetUser(userResponse.data.id, credentials.username, credentials.password);
                
                props.onSetTasks(userResponse.data.tasks);
                props.history.push("/viewCalendar");
            })
            .catch(err=> console.error("there was an issue logging in :("))
    }

    return <LoginForm onChange={handleChange} onSubmit={handleSubmit}></LoginForm>;
}

export default withRouter(Login);