import { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
//TODO: Make the function that actually submits this form to the server
//TODO: After a user logs in, they should be taken to their calendar view
//TODO: After a user logs in, we should get all of their tasks and use it to render the calendar view
export default function LoginForm(props){
    return(
        <div className="container">
            <Form onSubmit={props.onSubmit}>
                <Form.Group className="mb-3">
                    <Form.Label>Username</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Username"
                        name="username"
                        onChange={props.onChange}
                        />
                
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        name="password"
                        onChange={props.onChange}
                        />
                </Form.Group>

                <Button type="submit" variant="primary">Log In</Button>
            </Form>

            <Button variant="secondary" href="/register">Register a new account instead</Button>
        </div>
    );
}