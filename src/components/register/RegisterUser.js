import { useState } from "react";
import { withRouter } from "react-router-dom";
import RegisterUserForm from "./RegisterUserForm";
import axios from "axios";

//TODO: After a user is registered, they should be logged in and taken to the calendar view
function RegisterUser(props){
    const [credentials, setCredentials] = useState("");

    const handleChange = (e)=>{
        const {name, value} = e.target;
        setCredentials({...credentials, [name]:value});
    }

    const handleSubmit = (e)=>{
        e.preventDefault();

        const requestBody = `username=${credentials.username}&password=${credentials.password}`;
        console.log(requestBody);
        axios.post("http://3.230.1.183:8082/registerUser",requestBody, {headers:{"Content-Type": "application/x-www-form-urlencoded"}})
            .then(response=>{
                console.log("successfully registered a new account!!!");
                props.history.push("/");
            })
            .catch(err=> console.error("there was an issue registering an account :("))
    }
    return<RegisterUserForm onChange={handleChange} onSubmit={handleSubmit}></RegisterUserForm>;
}

export default withRouter(RegisterUser);