import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export default function RegisterUserForm(props){
    return (<div className="container">
            <Form onSubmit={props.onSubmit}>
                <Form.Group className="mb-3">
                    <Form.Label>Username</Form.Label>
                    <Form.Control 
                        type="text" 
                        name="username"
                        placeholder="Username"
                        onChange={props.onChange}
                        />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="text" 
                        name="password"
                        placeholder="Password" 
                        onChange={props.onChange}
                        />
                </Form.Group>

                <Button type="submit" variant="success">Register</Button>
            </Form>

            <Button variant="secondary" href="/login">Log in instead</Button>
        </div>);
}