import axios from "axios";
import { useState } from "react";
import { withRouter } from "react-router-dom";
import CreateTaskForm from "./CreateTaskForm";

function CreateTask(props){
    const [taskInfo, setTaskInfo] = useState("");

    const handleChange = (e)=>{
        const {name, value} = e.target;
        setTaskInfo({...taskInfo, [name]:value});
    }

    const handleAddSubtask = (subtask)=>{
        console.log(`Adding subtask to taskInfo, ${subtask}`);
        console.log(`taskInfo: ${JSON.stringify(taskInfo)}`);
        if(taskInfo.sub_tasks){
            setTaskInfo({...taskInfo, sub_tasks:[...taskInfo.sub_tasks, {info: subtask, completed: false}]});
            console.log(`Added new subtask: ${subtask}, all subtasks: ${taskInfo.sub_tasks}`);
        } else{
            setTaskInfo({...taskInfo, sub_tasks:[{info: subtask, completed: false}]});
            console.log(`Added first subtask: ${subtask}`)
        }
    }

    const handleDeleteSubtask =(subtask)=>{
        let newSubtaskList = taskInfo.sub_tasks.filter((_subtask)=>subtask!=_subtask.info);
        console.log(`deleting subtask: ${JSON.stringify(subtask)} taskInfo.subtasks: ${JSON.stringify(taskInfo.sub_tasks)}, newSubtaskList: ${JSON.stringify(newSubtaskList)}`);
        setTaskInfo({...taskInfo, sub_tasks:newSubtaskList});
    }

    const handleSubmit = (e)=>{
        e.preventDefault();

        const requestBody = {
            task_info: taskInfo.task_info,
            taskDate: taskInfo.taskDate,
            startTime: `${taskInfo.taskDate} ${taskInfo.startTime}:00`,
            endTime: `${taskInfo.taskDate} ${taskInfo.endTime}:00`,
            completed: false};

        console.log(requestBody);
        console.log(props.user);

        let taskId = -1;
        axios.post(
            //This part adds in the task
            `http://3.230.1.183:8082/tasks?id=${props.user.userId}`,
            requestBody, 
            {headers:{"Content-Type": "application/json", "Authorization": `${props.user.username}-auth-token`}})
            .then(response=>{
                console.log("successfuly added task!!!");
                console.log(response);
                taskId = response.data.id;
                //props.history.push("/viewCalendar");
            })
            .then(response=>{
                //This part loops through all of the subtasks if there are any, and adds them to the task we just created
                if(taskInfo.sub_tasks){
                    taskInfo.sub_tasks.map(subtask=>{
                        const subtaskRequestBody = subtask;
                        axios.post(
                            `http://3.230.1.183:8082/SubTasks?id=${taskId}`,
                            subtaskRequestBody)
                        .then(response=>{
                            console.log("successfully added subtask!!!");
                        })
                    })
                }
            })
            .then(response=>{
                //This part uses the setTasks function from App.js to rerender the tasks with the new one added on
                console.log(`taskInfo in createTask: ${taskInfo}`);
                let newTask = taskInfo;
                props.onSetTasks([...props.tasks, taskInfo]);
                setTaskInfo({task_info: "", taskDate: taskInfo.taskDate, startTime: "", endTime: "", completed: false, sub_tasks: []});
                props.toggleTaskForm();
                props.history.push("/viewCalendar");
            })
            .catch(err=> console.error("there was an issue logging in :(", err))
    }

    return <CreateTaskForm 
        onChange={handleChange} 
        onSubmit={handleSubmit} 
        onAddSubtask={handleAddSubtask}
        onDeleteSubtask={handleDeleteSubtask}
        taskInfo={taskInfo}
        subtaskList={taskInfo.sub_tasks}
        />;
}

export default withRouter(CreateTask);