import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Collapse } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { Fade } from "react-bootstrap";
import { Alert } from "react-bootstrap";
import { useState } from "react";

export default function CreateTaskForm(props){

    let [openCreateSubtaskForm, setopenCreateSubtaskForm] = useState(false);
    let toggleOpenCreateSubtaskForm= () => {
        setopenCreateSubtaskForm(!openCreateSubtaskForm);
    }

    let [currentSubtask, setCurrentSubtask] = useState("");

    let cardStyle= {display: 'flex', width: '18rem', margin: 'auto'}

    return(
        <Card
        bg="light"
        text="dark"
        style={cardStyle}
        className="mb-2"
        >
        <Card.Body>
            <Form onSubmit={props.onSubmit}>
                <Form.Group className="mb-3">
                    <Form.Label>Task Information:</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Task text"
                        name="task_info"
                        value={props.taskInfo.task_info}
                        onChange={props.onChange}
                        />
                    
                    <Form.Label>Date:</Form.Label>
                    <Form.Control 
                        type="date" 
                        placeholder="Date"
                        name="taskDate"
                        value={props.taskInfo.taskdate}
                        onChange={props.onChange}
                        />
                    
                    <Form.Label>Start Time:</Form.Label>
                    <Form.Control 
                        type="time" 
                        placeholder="Start Time"
                        name="startTime"
                        value={props.taskInfo.startTime}
                        onChange={props.onChange}
                        />
                    
                    <Form.Label>End Time:</Form.Label>
                    <Form.Control 
                        type="time" 
                        placeholder="End Time"
                        name="endTime"
                        value={props.taskInfo.endTime}
                        onChange={props.onChange}
                        />

                    {/*Chooses whether to show the button to add a subtask or to submit subtask based on whether
                    or not the form is already open*/}
                    {openCreateSubtaskForm ? 
                    <>
                        <Button 
                            variant="outline-primary" 
                            onClick={()=>{
                                setCurrentSubtask("");
                                props.onAddSubtask(currentSubtask);
                                }}
                        > 
                        Add Subtask
                        </Button>
                        <Button variant="outline-secondary" onClick={toggleOpenCreateSubtaskForm}>Cancel</Button>
                    </> 
                    : 
                    <Button variant="outline-primary" onClick={toggleOpenCreateSubtaskForm}>Create New Subtask</Button>
                    }
                    
                    {/*Opens substask form based on whether or not the button has been clicked*/}
                    <Fade in={openCreateSubtaskForm}>
                    <Form.Control 
                        type="text" 
                        placeholder="Subtask" 
                        name="password"
                        value={currentSubtask}
                        onChange={(e) => setCurrentSubtask(e.target.value)}
                        />

                    </Fade>

                    {/*Shows the subtasks that have already been added */}
                    {props.subtaskList ?
                    props.subtaskList.map((subtask, index)=>{
                    
                    return(<Alert key={index} variant="info" onClose={() => props.onDeleteSubtask(subtask.info)} dismissible>
                        {subtask.info}
                    </Alert>)
                    }) 
                    : 
                    null}
                </Form.Group>


                <Button type="submit" variant="success">Create Task</Button>
            </Form>

        </Card.Body>
        </Card>
    );
}