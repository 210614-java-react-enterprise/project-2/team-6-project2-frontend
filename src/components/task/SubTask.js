import { Alert } from "react-bootstrap"

export default function SubTask(props){
    console.log(`props in Subtask: ${JSON.stringify(props)}`);
    console.log(`props.subtask: ${props.subtask}`);
    return   <Alert variant="primary"> {props.subtask.info}</Alert>
}