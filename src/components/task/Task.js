import { Card } from "react-bootstrap";
import SubTask from "./SubTask";

export default function Task(props){
    console.log(`props in Task: ${JSON.stringify(props)}`)
    return(

        
    <Card
    bg="success"
    text="white"
    style={{ width: '18rem' }}
    className="mb-2"
    >
        <Card.Body>
            <Card.Title>{props.task.task_info}</Card.Title>
            <Card.Subtitle>Start Time:</Card.Subtitle>
                <Card.Text>{props.task.startTime.length > 6 ? props.task.startTime.substring(11,16) : props.task.startTime}</Card.Text>
            <Card.Subtitle>End Time:</Card.Subtitle>
                <Card.Text>{props.task.endTime.length > 6 ? props.task.endTime.substring(11,16) : props.task.endTime}</Card.Text>
            {props.task.sub_tasks ? 
                props.task.sub_tasks.length > 0 ? 
                    <Card.Subtitle>Sub Tasks: </Card.Subtitle> :
                    null
                    :
                null
            }
            {
            props.task.sub_tasks ? 
                props.task.sub_tasks.map((subtask, index)=> <SubTask key={index} subtask={subtask}></SubTask>) : 
                null
            }
        </Card.Body>
    </Card>
    );
}